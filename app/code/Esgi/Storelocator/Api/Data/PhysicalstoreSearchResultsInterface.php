<?php
namespace Esgi\Storelocator\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for job physicalstore search results.
 * @api
 */
interface PhysicalstoreSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get physicalstores list.
     *
     * @return \Esgi\Storelocator\Api\Data\PhysicalstoreInterface[]
     */
    public function getItems();

    /**
     * Set physicalstores list.
     *
     * @param \Esgi\Storelocator\Api\Data\PhysicalstoreInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
