<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Esgi\Storelocator\Model;

use Esgi\Storelocator\Api\PhysicalstoreRepositoryInterface;
use Esgi\Storelocator\Api\Data;
use Esgi\Storelocator\Model\ResourceModel\Physicalstore as ResourcePhysicalstore;
use Esgi\Storelocator\Model\ResourceModel\Physicalstore\CollectionFactory as PhysicalstoreCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;

/**
 * Class BlockRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PhysicalstoreRepository implements PhysicalstoreRepositoryInterface
{
    /**
     * @var ResourcePhysicalstore
     */
    protected $resource;

    /**
     * @var PhysicalstoreFactory
     */
    protected $physicalstoreFactory;

    /**
     * @var PhysicalstoreCollectionFactory
     */
    protected $physicalstoreCollectionFactory;

    /**
     * @var Data\PhysicalstoreSearchResultsInterface
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Esgi\Storelocator\Api\Data\PhysicalstoreInterfaceFactory
     */
    protected $dataPhysicalstoreFactory;

    /**
     * @param ResourcePhysicalstore $resource
     * @param PhysicalstoreFactory $physicalstoreFactory
     * @param Data\PhysicalstoreInterfaceFactory $dataPhysicalstoreFactory
     * @param PhysicalstoreCollectionFactory $physicalstoreCollectionFactory
     * @param Data\PhysicalstoreSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     */
    public function __construct(
        ResourcePhysicalstore $resource,
        PhysicalstoreFactory $physicalstoreFactory,
        \Esgi\Storelocator\Api\Data\PhysicalstoreInterfaceFactory $dataPhysicalstoreFactory,
        PhysicalstoreCollectionFactory $physicalstoreCollectionFactory,
        Data\PhysicalstoreSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor
    ) {
        $this->resource = $resource;
        $this->physicalstoreFactory = $physicalstoreFactory;
        $this->physicalstoreCollectionFactory = $physicalstoreCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPhysicalstoreFactory = $dataPhysicalstoreFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
    }

    /**
     * Save Physicalstore data
     *
     * @param \Esgi\Storelocator\Api\Data\PhysicalstoreInterface $physicalstore
     * @return Physicalstore
     * @throws CouldNotSaveException
     */
    public function save(Data\PhysicalstoreInterface $physicalstore)
    {
        try {
            $this->resource->save($physicalstore);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $physicalstore;
    }

    /**
     * Load Physicalstore data by given Physicalstore Identity
     *
     * @param string $physicalstoreId
     * @return Physicalstore
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($physicalstoreId)
    {
        $physicalstore = $this->physicalstoreFactory->create();
        $this->resource->load($physicalstore, $physicalstoreId);
        if (!$physicalstore->getId()) {
            throw new NoSuchEntityException(__('Storelocator Physicalstore with id "%1" does not exist.', $physicalstore));
        }

        return $physicalstore;
    }

    /**
     * Load Physicalstore data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Esgi\Storelocator\Api\Data\PhysicalstoreSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Esgi\Storelocator\Model\ResourceModel\Physicalstore\Collection $collection */
        $collection = $this->physicalstoreCollectionFactory->create();

        /** @var Data\PhysicalstoreSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * Delete Physicalstore
     *
     * @param \Esgi\Storelocator\Api\Data\PhysicalstoreInterface $physicalstore
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\PhysicalstoreInterface $physicalstore)
    {
        try {
            $this->resource->delete($physicalstore);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Physicalstore by given Physicalstore Identity
     *
     * @param string $physicalstoreId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($physicalstoreId)
    {
        return $this->delete($this->getById($physicalstoreId));
    }
}
