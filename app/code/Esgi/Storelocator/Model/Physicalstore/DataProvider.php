<?php
namespace Esgi\Storelocator\Model\Physicalstore;

use Esgi\Storelocator\Model\ResourceModel\Physicalstore\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Esgi\Storelocator\Model\ResourceModel\Physicalstore\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $physicalstoreCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $physicalstoreCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $physicalstoreCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Esgi\Storelocator\Model\Physicalstore $physicalstore */
        foreach ($items as $physicalstore) {
            $this->loadedData[$physicalstore->getId()] = $physicalstore->getData();
        }

        $data = $this->dataPersistor->get('storelocator_physicalstore');

        if (!empty($data)) {
            $physicalstore = $this->collection->getNewEmptyItem();
            $physicalstore->setData($data);
            $this->loadedData[$physicalstore->getId()] = $physicalstore->getData();
            $this->dataPersistor->clear('storelocator_physicalstore');
        }

        return $this->loadedData;
    }
}
