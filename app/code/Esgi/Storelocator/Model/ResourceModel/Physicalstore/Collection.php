<?php
namespace Esgi\Storelocator\Model\ResourceModel\Physicalstore;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Esgi\Storelocator\Model\Physicalstore', 'Esgi\Storelocator\Model\ResourceModel\Physicalstore');
    }

}
